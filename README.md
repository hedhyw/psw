# README #

Password generator uses /dev/urandom to get random bytes.

### How do I use it? ###
* `psw [length] [using symbols] [count of passwords]`
* `psw 10 f2 2` generates two passwords like mavu9hke5d
* `psw 6 Ap` generates password like HuvbZk

### How do I install it? ###

* `make`
* `make install`
* `make clean`

### How do I remove it? ##
* `make unistall`