LONG_BIT = $(shell getconf LONG_BIT)
ifeq ($(LONG_BIT),64)
	FLAGS=-m64
else
	FLAGS=-m32
endif
all:
	g++ psw.cxx -o psw -std=gnu++11 -Ofast -march=native $(FLAGS)
install:
	cp psw /usr/local/bin -i
uninstall:
	rm /usr/local/bin/psw -i
clean:
	rm -rf *.o run
	
