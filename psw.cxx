// `psw 8 a0` -> `jh2x52yx`
// g++ (Ubuntu 4.8.4-2ubuntu1~14.04) 4.8.4
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;
const int maxPswLen = 2048;
const int randMaxPswLen = 24;
const int randMinPswLen = 14;
const string lowch = "abcdefghijklmnopqrstuvwxyz";
const string uppch = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
const string numch = "0123456789";
int main(int argc, char **argv)
{
	int length = 0;
	string chars="";
	char* buffer = new char[maxPswLen];
	ifstream ifs("/dev/urandom", ios_base::in | ios_base::binary);
	// length
	if (argc >= 2) length = atoi(argv[1]);
	if (length < 1 or length >= maxPswLen)
	{
		length = ((unsigned char)ifs.get() % (randMaxPswLen-randMinPswLen)+randMinPswLen);
	}
	// chars
	if (argc >= 3)
	{
		for (int X = 0;argv[2][X]!=0;++X)
		{
			if (chars.find(argv[2][X]) != string::npos)
				continue;
			if (lowch.find(argv[2][X]) != string::npos)
				chars += lowch;
			else if (uppch.find(argv[2][X]) != string::npos)
				chars += uppch;
			else if (numch.find(argv[2][X]) != string::npos)
				chars += numch;
			else
				chars += argv[2][X];
		}
	}
	if (chars == "") chars = lowch+uppch+numch;
	// count
	int pswCount = 1;
	if (argc >= 4)
	{
		pswCount = atoi(argv[3]);
		if (pswCount < 1) pswCount = 1;
	}
	// generates
	for (int X = 0; X < pswCount; ++X)
	{
		string psw = "";
		ifs.read(buffer,length);
		for (int Y = 0; Y < length; psw += chars[buffer[Y++] % chars.length()]);
		cout << psw << endl;
	}
	ifs.close();
	delete [] buffer;
	return 0;
}
/* Geany 1.23.1 editor */
